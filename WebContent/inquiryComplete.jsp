<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inquiry Complete</title>
</head>
<body>
<s:property value="name"/>さん、問い合わせありがとうございました。<br>
問い合わせの種類:<br>
<s:if test='qtype=="company"'>会社について</s:if>
<s:if test='qtype=="product"'>製品について</s:if>
<s:if test='qtype=="support"'>アフターサポート</s:if><br>
問い合わせの内容:
<s:property value="body"/>
</body>
</html>